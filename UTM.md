Schritte
=========

1. [UTM](https://mac.getutm.app/) herunterladen und installieren
2. [Ubuntu 24.04 Desktop for ARM](http://cdimage.ubuntu.com/noble/daily-live/current/noble-desktop-arm64.iso) herunterladen und installieren ([1](https://docs.getutm.app/guides/ubuntu/))
3. Nach dem Neustart einloggen und MATE installieren - *WICHTIG* bei
der Frage nach dem Login-Manager ```lightdm``` auswählen: 
```
sudo apt install -y ubuntu-mate-desktop
```
4. Die VM neu starten, in der Anmeldeoberfläche (das braucht es nur einmal) über das Ubuntu-Symbol die Option MATE auswählen und einloggen
5. Ein Terminal starten und das Installationsskript ausführen (s. [README](README.md))
6. Nacharbeiten: Tastaturlayout und Sprache einstellen
Die Tastaturbelegung auf Apple->Macbook Pro umstellen und bei Optionen im ersten Abschnitt den zweiten Punkt auswählen.