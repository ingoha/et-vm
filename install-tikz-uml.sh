#!/bin/bash

# Autor: Ingo Haschler <lehre@itih.de>

TIKZUML_VERSION="v1.0-2016-03-29"
TIKZUML_FILENAME=tikzuml-${TIKZUML_VERSION}.tbz
TIKZUML_URL="https://perso.ensta-paris.fr/~kielbasi/tikzuml/var/files/src/${TIKZUML_FILENAME}"

pushd /tmp
wget $TIKZUML_URL
tar xf ${TIKZUML_FILENAME}
mkdir -p ~/texmf/tex/latex
cp tikzuml-${TIKZUML_VERSION}/tikz-uml.sty ~/texmf/tex/latex
popd
