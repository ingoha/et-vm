#!/bin/bash
# Einfaches setup.sh um eine LinuxMint Mate-Installation
# für den Einsatz als VM im Studiengang ET zu konfigurieren
#
# Autor: Ingo Haschler <lehre@itih.de>

#
# TBD: Reihenfolge prüfen
#

# Ideen
# Ganz von vorne: http://askubuntu.com/questions/122505/how-do-i-create-completely-unattended-install-for-ubuntu

# Generell keine Abfragen
export DEBIAN_FRONTEND=noninteractive

# Abkürzungen
INSTALL="sudo -E apt install -y"
UPDATE="sudo -E apt update -qq"
UPGRADE="sudo -E apt upgrade -fuy"
DISTUPGRADE='sudo -E apt -y dist-upgrade -o Dpkg::Options::="--force-confnew"'

# Hilfsfunktionen
function qemu(){
  sudo dmidecode -s system-product-name | grep -q QEMU
}

function ubuntu(){
  grep -q Ubuntu /etc/lsb-release
}

function aarch64(){
  [[ $(arch) == 'aarch64' ]]
}

# Partner-Repository aktivieren
sudo sed -i "/^# deb .*partner/ s/^# //" /etc/apt/sources.list
$UPDATE

# Dateiaustausch
if qemu
then
  $INSTALL spice-vdagent qemu-guest-agent spice-webdavd
# FIXME path???
  sudo mkdir /media/hgfs
  sudo chmod a+w /media/hgfs
  echo 'Shared	/media/hgfs	9p	trans=virtio,version=9p2000.L,noauto,user	0 0' | sudo tee -a /etc/fstab
else
  # open-vm-tools statt VMware-Tools
  $INSTALL open-vm-tools-desktop
  # cf. https://ericsysmin.com/2017/05/24/allow-hgfs-mount-on-open-vm-tools/
  sudo mkdir /media/hgfs
  sudo chmod a+w /media/hgfs
  echo '.host:/ /media/hgfs fuse.vmhgfs-fuse noauto,user 0 0' | sudo tee -a /etc/fstab
fi
# FIXME was ist mit Virtualbox? https://help.ubuntu.com/community/VirtualBox/GuestAdditions
# cf. https://unix.stackexchange.com/questions/89714/easy-way-to-determine-the-virtualization-technology-of-a-linux-machine

# Sprachpakete installieren und Sprache auf deutsch umstellen
# /etc/environment, /etc/default/locale
# s. https://help.ubuntu.com/community/Locale#Changing_settings_permanently
# und http://wiki.ubuntuusers.de/Spracheinstellungen
$INSTALL language-pack-de language-pack-gnome-de language-pack-kde-de
sudo update-locale LANG=de_DE.UTF-8 LC_MESSAGES=de_DE.UTF-8
# /etc/default/console-setup
sudo sed -i "/^XKBMODEL.*/ s/evdev/pc105/" /etc/default/console-setup
sudo sed -i "/^XKBLAYOUT.*/ s/us/de/" /etc/default/console-setup
# und die Zeitzone
# http://superuser.com/questions/498330/changing-timezone-with-dpkg-reconfigure-tzdata-and-debconf-set-selections
sudo sh -c 'echo "Europe/Berlin" > /etc/timezone'
sudo dpkg-reconfigure -f noninteractive tzdata

$UPGRADE
$DISTUPGRADE

$INSTALL mysql-server
sudo mysql -u root --execute="ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '123456';"

# Code::Blocks
$INSTALL codeblocks codeblocks-contrib
mkdir -p $HOME/.config/codeblocks
cp .config/codeblocks/default.conf $HOME/.config/codeblocks

# VS Code
# cf. https://www.ubuntuupdates.org/ppa/vscode
pushd /tmp
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
popd
$INSTALL apt-transport-https
sudo sh -c 'echo "deb https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
$UPDATE
$INSTALL code
# extensions
code --force --install-extension MS-CEINTL.vscode-language-pack-de
code --force --install-extension ms-vscode.cpptools
# Font im Terminal (#44)
mkdir -p ~/.config/Code/User
echo "{" > ~/.config/Code/User/settings.json
echo '"terminal.integrated.fontFamily": "Source Code Pro"' >> ~/.config/Code/User/settings.json
echo "}" >> ~/.config/Code/User/settings.json

# Zusätzliche Pakete installieren
# TBD: Mit package.list abgleichen; sind größtenteils schon enthalten...
# C(++) und Co.
MISC_PACKAGES="build-essential cppcheck"
# SQLite
MISC_PACKAGES="$MISC_PACKAGES sqlite3 sqlitebrowser"
# MySQL
MISC_PACKAGES="$MISC_PACKAGES libmysql++-dev default-libmysqlclient-dev libmysql++-dev libmysql++-doc libmysql++3v5 mysql-client"
$INSTALL $MISC_PACKAGES
# sql-workbench
pushd /tmp
wget https://www.sql-workbench.eu/archive/Workbench-Build129-with-optional-libs.zip
sudo mkdir /opt/sqlworkbench
sudo chown ${USER}:${USER} /opt/sqlworkbench
unzip /tmp/Workbench-Build129-with-optional-libs.zip -d /opt/sqlworkbench
mkdir /opt/sqlworkbench/drivers
wget https://downloads.mariadb.com/Connectors/java/connector-java-2.7.9/mariadb-java-client-2.7.9.jar
mv mariadb-java-client-2.7.9.jar /opt/sqlworkbench/drivers
popd
# config
cat <<EOF > ~/.local/share/applications/sqlworkbench.desktop
[Desktop Entry]
Categories=
Comment=An ISQL tool
Comment[de]=An ISQL tool
Encoding=UTF-8
Exec=/opt/sqlworkbench/sqlworkbench.sh
GenericName=
GenericName[de]=
Icon=/opt/sqlworkbench/workbench32.png
MimeType=
Name=SQL Workbench
Name[de]=SQL Workbench
Path=/opt/sqlworkbench
ServiceTypes=
SwallowExec=
SwallowTitle=
Terminal=
TerminalOptions=
Type=Application
URL=
EOF
mkdir ~/.sqlworkbench
cat <<EOF > ~/.sqlworkbench/wb-profiles.properties
profile.0001.name=MySQL lokal
profile.0001.autocommmit=false
profile.0001.driverclass=org.mariadb.jdbc.Driver
profile.0001.drivername=MariaDB
profile.0001.password=123456
profile.0001.store.pwd=true
profile.0001.tags=
profile.0001.url=jdbc:mariadb://localhost
profile.0001.username=root
EOF
cp .config/sqlworkbench/WbDrivers.xml ~/.sqlworkbench
# SE
SE_PACKAGES="libunittest++-dev libtool automake meld doxygen umbrello kdiff3"
# cf. https://bugs.launchpad.net/ubuntu/+source/umbrello/+bug/1585611
SE_PACKAGES="$SE_PACKAGES dbus-user-session kinit kio kio-extras kded5"
$INSTALL $SE_PACKAGES
# Latex
LATEX_PACKAGES="texmaker texlive-lang-german texlive-latex-extra texlive-extra-utils texlive-bibtex-extra jabref texlive-fonts-recommended texlive-latex-recommended python3-pygments"
if ! ubuntu; then
  LATEX_PACKAGES="$LATEX_PACKAGES xreader"
fi
# FIXME evtl. https://de.linux-terminal.com/?p=6644 für ubuntu?
$INSTALL $LATEX_PACKAGES
mkdir -p $HOME/.config/xm1
cp .config/xm1/texmaker.ini $HOME/.config/xm1
# Zotero (issue 58); nur für x86 :-/
if ! aarch64; then
pushd /tmp
wget https://www.zotero.org/download/client/dl\?channel\=release\&platform\=linux-x86_64\&version\=6.0.35 -O zotero.tar.xz
sudo mkdir /opt/zotero
sudo chown ${USER}:${USER} /opt/zotero
tar xf zotero.tar.xz -C /opt/zotero
popd
cp zotero.desktop ~/.local/share/applications
fi
# Sonstiges
# Preseed wireshark config
echo wireshark-common wireshark-common/install-setuid boolean true | sudo debconf-set-selections
$INSTALL curl gnuplot gnuplot-x11 wireshark ngspice ngspice-doc vim traceroute dconf-cli nmap

# Issue 18
$INSTALL logisim

# Issue 56; nur für x86 :-/
if ! aarch64; then
$INSTALL wine
pushd /tmp
wget https://ltspice.analog.com/software/LTspice64.msi
# FIXME WINEPREFIX?
WINEARCH=win64 wine msiexec /i LTspice64.msi /qn 
rm $(xdg-user-dir DESKTOP)/LTspice*
popd
fi
pushd /tmp
wget https://github.com/hneemann/Digital/releases/latest/download/Digital.zip
sudo unzip Digital.zip -d /opt
pushd /opt/Digital
./install.sh
popd
popd

# Issue 40
$INSTALL zsh
sudo chsh $USER -s /bin/zsh
cp /etc/zsh/newuser.zshrc.recommended .zshrc
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh) --unattended"

# Issue 51 (QEMU)
$INSTALL qemu-utils qemu-system-arm

# NodeJS u.a.
# cf. https://github.com/nodesource/distributions#installation-instructions
$INSTALL ca-certificates curl gnupg python3-venv
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
NODE_MAJOR=22
echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | sudo tee /etc/apt/sources.list.d/nodesource.list
$UPDATE
$INSTALL nodejs

# Docker
# cf. https://linuxiac.com/how-to-install-docker-on-linux-mint-22/
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu noble stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
$UPDATE
$INSTALL docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Schrift SourceCodePro installieren
# http://askubuntu.com/questions/193072/how-to-use-the-new-adobe-source-code-pro-font
mkdir /tmp/adobefont
pushd /tmp/adobefont
wget -O f.zip "https://github.com/adobe-fonts/source-code-pro/releases/download/2.042R-u%2F1.062R-i%2F1.026R-vf/OTF-source-code-pro-2.042R-u_1.062R-i.zip"
unzip -o -j f.zip
mkdir -p ~/.fonts
cp *.otf ~/.fonts
popd
fc-cache -f -v
# und aktivieren
# geht neuerdings mit dconf:
# http://wiki.ubuntuusers.de/GNOME_Konfiguration/dconf
dconf write /org/mate/terminal/profiles/default/font "'Source Code Pro Regular 12'"
dconf write /org/mate/terminal/profiles/default/use-system-font false

# Evtl. vmware-toolbox per Autostart
#echo "vmware-toolbox & >> ~/.xsession"

# Optional: Password-Prompt
# Dazu neue Datei pwfeedback in /etc/sudoers.d mit dem Inhalt:
# Defaults pwfeedback

# Sonstiges:
# TBD

# Einstellungen für Umbrello
mkdir -p ~/.kde/share/config
cp ./umbrellorc ~/.kde/share/config/umbrellorc

# Verknüpfung für Proxy-Einstellungen
#cp /usr/share/applications/mate-network-properties.desktop ~/Schreibtisch
#sudo chmod a+x ~/Schreibtisch/mate-network-properties.desktop

# Alte Kernels automatisch entfernen
#sudo sed -i "/linux-image/ s/^/#/" /etc/apt/apt.conf.d/01autoremove
#sudo sed -i "/Remove-Unused-Dependencies/ s/^\/\///" /etc/apt/apt.conf.d/50unattended-upgrades

# Release-Updates nicht anzeigen
#sudo sed -i "/Prompt/ s/^.*$/Prompt=never/" /etc/update-manager/release-upgrades

## AVR-GCC installieren
$INSTALL gcc-avr avr-libc
mkdir ~/bin
cp ./bin/* ~/bin
chmod +x ~/bin/*
sudo usermod -a -G dialout $USER

cd doku
pdflatex --shell-escape vm-101.tex
cp vm-101.pdf "Dokumentation zur ET-VM.pdf"
mv "Dokumentation zur ET-VM.pdf" $(xdg-user-dir DESKTOP)

# dotfiles installieren
cd $HOME
#ln -sb dotfiles/.screenrc .
ln -sb et-vm/.bash_profile .
ln -sb et-vm/.bashrc .
ln -sb et-vm/.bashrc_custom .
#ln -sf dotfiles/.emacs.d .

sudo apt clean
