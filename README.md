Installation
=========
Ein frisches LinuxMint 22 (Mate) installieren. Für Mint 22 sollte die virtuelle Festplatte mindestens 30GB (besser 50GB) groß sein.

Die Referenz-Instanz wurde mit diesem (https://www.linuxmint.com/edition.php?id=318) Installationsmedium erzeugt, auf der im Anschluß zuerst alle Pakete aktualisiert wurden. Als Benutzer wurde student mit dem Paßwort 123456 eingestellt. Optional sollte die Festplatten-Verschlüsselung (WDE) sowie LVM aktiviert werden.

<img src="screenshots/Bildschirmfoto vom 2022-01-02 14-25-27.png" width=50% height=50%>
<img src="screenshots/Bildschirmfoto vom 2022-01-02 14-25-49.png" width=50% height=50%>
<img src="screenshots/Bildschirmfoto vom 2022-01-02 14-26-07.png" width=50% height=50%>
<img src="screenshots/Bildschirmfoto vom 2022-01-02 14-26-55.png" width=50% height=50%>

Dann klonen und ausführen, um die Arbeitsumgebung einzurichten:

```sh
cd $HOME
sudo apt install -y git
git clone https://gitlab.com/ingoha/et-vm.git
cd et-vm
./setup.sh
```

Wird das Image auf einer USB-Platte betrieben, sollte in der Konfigurationsdatei (.vmx) folgende Zeile eingefügt werden:
```mainMem.useNamedFile = "FALSE"```



Weitere Pakete
=

PapDesigner
-

In der Kommandozeile:

```sh
cd ~/et-vm
git pull
./install-papdesigner.sh
```

Bitte bei Aufforderung das Benutzerpasswort (Standard: 123456) eingeben.
**Achtung:** Das ganze dauert ziemlich lang, außerdem sind ein paar Meldungen zu beantworten (s. Bildschirmfotos).

<img width=50% height=50% src="/uploads/df323a226466f986690fb3d7fd2762ea/Bildschirmfoto_zu_2021-12-30_15-44-52.png">
<img width=50% height=50% src="/uploads/9937006c15fd0861d279b658126f87ba/Bildschirmfoto_zu_2021-12-30_15-51-33.png">
<img width=50% height=50% src="/uploads/006ac5d0f4c6c1f405fcd708a75e66f1/Bildschirmfoto_zu_2021-12-30_15-52-37.png">
<img width=50% height=50% src="/uploads/e857d4ad45c5151c7b27ffee81bb2667/Bildschirmfoto_zu_2021-12-30_15-52-48.png">
<img width=50% height=50% src="/uploads/bf2332aed78a5b7df29cd7194bfa3ccc/Bildschirmfoto_zu_2021-12-30_15-57-59.png">
<img width=50% height=50% src="/uploads/004efe3db939ed96d0e1c8eb93e52bad/Bildschirmfoto_zu_2021-12-30_15-58-09.png">
