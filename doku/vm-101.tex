\documentclass{scrartcl}

\usepackage{minted}
\usepackage[normalem]{ulem}

\usepackage{color}
\usepackage[pdftex]{graphicx}
\usepackage[percent]{overpic}
\usepackage{float}

\usepackage[labelfont=bf,textfont=it]{caption}

\usepackage{svn-multi}

\usepackage[german]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{german}
\usepackage[pdftex]{hyperref}
\hypersetup {
	pdftitle={Infos zur VM},
	pdfauthor={Ingo Haschler},
	colorlinks,
        urlcolor=blue,
        linkcolor=blue,
        citecolor=blue
}


\usepackage{tabularx,multirow}
\usepackage{booktabs}
\usepackage{float}

\usepackage{pdfpages}

\usepackage{eurosym}

\usepackage[square,numbers]{natbib}
\usepackage{bibtopic}
\bibliographystyle{alphadin}

% Für Quellcode
%\usepackage{minted}
%\usepackage{listings}
\definecolor{grau}{RGB}{245,245,245}
\definecolor{dunkelgrau}{RGB}{190,190,190}
\usepackage{bold-extra}


%\lstloadlanguages{java}
% define Eclipse-like colors and fonts
\definecolor{keyword}{rgb}{0.5,0.0,0.33}
\definecolor{comment}{rgb}{0.25,0.5,0.37}


\title{Infrastruktur-Einführung ET}
\subtitle{Informationen zur virtuellen Maschine}
\author{Dipl. Inform. Med. Ingo Haschler\thanks{lehre@itih.de}}
\date{\today}

\begin{document}

\maketitle


\section*{Einleitung}
Für Ihr Studium der Elektrotechnik erhalten Sie eine USB-Festplatte zur Verfügung gestellt. Auf dieser befindet sich eine speziell abgestimmte virtuelle Maschine. Diese wird an einem beliebigen PC mit einer Virtualisierungssoftware (\href{http://www.vmware.com/}{VMware}) betrieben. Auf den meisten PCs innerhalb der Hochschule ist sie installiert, so daß Sie dort nur die Festplatte einstecken müssen und dann Ihre virtuelle Maschine benutzen können.

Die Aufgabe einer Virtualisierungssoftware ist es, alle Komponente eines PCs
nachzubilden\footnote{Genaugenommen ist dies ein Spezialfall der
Virtualisierung, der aber für \emph{VMware Player} zutrifft.} Das Betriebssytem, das die
Virtualisierungssoftware ausführt, nennt man \emph{Wirtssystem}.

Innerhalb einer virtuellen Maschine (VM) kann man nun wie auf einem physischen
PC arbeiten. Auch ein eigenes BIOS ist vorhanden, so daß auch der Bootvorgang
gesteuert werden kann. Insbesondere kann man ein beliebiges Betriebssystem
installieren, Grenzen bietet hier nur die Leistung und der Prozessortyp des Wirts.

\subsection*{Anforderungen}
Für die Nachbildung der Hardwarekomponenten wird zusätzliche Prozessor-Leistung 
im Wirt benötigt, die weder für den Wirt noch für den Gast bereitstehen. 
Moderne Virtualisierungen benötigen aber nur ca. 3\% Overhead. Heutige 
Prozessoren haben auch kein Problem damit, mehrere Betriebssysteme gleichzeitig 
zu bedienen. Eine viel wichtigere Rolle spielt der Hauptspeicher des Wirts, von 
dem ein festgelegter Teil für den Gast reserviert wird. Er sollte daher für den 
Betrieb großzügig bemessen sein.

\subsection*{Virtuelle Festplatten}
Auch der Festplattenplatz des Wirts sollte für den VM-Betrieb ausreichend
dimensioniert sein. Man kann in eine VM wie in einen physischen PC eine oder
mehrere Festplatten ``einbauen'', diese finden sich im Dateisystem des Wirts als
sogenannte \emph{Abbilder} wieder\footnote{Diese Abbilder haben bei
\emph{VMware} die Endung \texttt{.vmdk}}. 

\subsection*{Optische Datenträger}
Zusätzlich zu physischen optischen Datenträgern (CDs und DVDs) kann man in eine
VM auch Abbilder derselben ``einlegen''. Hierzu muß das Abbild als
\emph{ISO-Image} vorliegen; erkennbar an der Endung \texttt{.iso}. Wenn man z.B.
den Installationsdatenträger einer Linux-Distribution aus dem Internet lädt, liegt
sie meist in diesem Format vor. Außerdem kann man mit der geeigneten Software
selbst von jedem physischen Datenträger ein solches Abbild erstellen.

\section*{VMware}
Ein führender Anbieter von Virtualisierungslösungen, auch und vor allem im 
Unternehmensumfeld, ist \href{https://www.vmware.com}{VMware}. Für 
Privatanwender bietet der Hersteller unter anderem den \href{https://www.vmware.com/products/workstation-player.html}{VMware Workstation Player} kostenlos an. 
Eine große Auswahl fertiger VMs findet sich im \href{https://marketplace.vmware.com/}{Virtual Appliance 
Marketplace}. Mit dem Player lassen sich auch eigene virtuelle Maschinen erstellen.

\subsection*{Installation}
Nachdem man sich das passende Paket von 
\href{http://www.vmware.com/download/player/}{www.vmware.com/download/player/} heruntergeladen hat, startet man die Installation. \emph{Tip:} Die Installationspakete können Sie auch \href{http://softwareupdate.vmware.com/cds/vmw-desktop/player/}{direkt} herunterladen, das spart den Umweg über die Webseite.

Ist der \emph{VMware Player} installiert, kann man vorhandene virtuelle
Maschinen einfach durch Doppelklick auf die Datei mit der Endung \texttt{.vmx} starten.

\subsection*{Alternative: VirtualBox}
Eine weitere (kostenlose) Möglichkeit, die virtuelle Maschine auszuführen, bietet \href{https://www.virtualbox.org/wiki/Downloads}{VirtualBox}. Da VMware nicht für MacOS verfügbar ist, ist VirtualBox hier das Mittel der Wahl. Weitere Informationen dazu finden Sie auf der letzten Seite.

\subsection*{Netzwerkkonfiguration}
Damit das Betriebssystem innerhalb der virtuellen Maschine mit der ``Außenwelt''
kommunizieren kann, benötigt es eine Netzwerkverbindung. Der \emph{VMware
Player} bietet in der Standardinstallation drei Netzwerkverbindungen an:
\begin{description}
	\item[Bridged] Die virtuelle Netzwerkkarte wird in das vorhandene Netzwerk
	eingeblendet, ist also auch von außerhalb sichtbar.
	\item[Host-only] Die virtuelle Netzwerkkarte wird mit einer Schnittstelle im
	Wirtsbetriebssystem verbunden. Auf diese Weise kann die VM ausschließlich mit
	dem Wirt kommunizieren.
	\item[NAT] Die virtuelle Netzwerkkarte wird zunächst mit dem Wirtssystem
	verbunden. Dieser leitet aber Anfragen an die Außenwelt weiter. Dabei wird der
	Gast durch die sogenannte \emph{Network Address Translation} maskiert (etwas 
	wie hinter einem DSL-Router).
\end{description}


\subsection*{Datenaustausch mir der virtuellen Maschine}
Die einfachste Möglichkeit ist der Austausch per Drag\&Drop zwischen der VM und dem Wirtssystem. Dies setzt eine korrekt konfigurierte Installation der VMware Tools voraus (s.a. weiter unten).

Über sogenannte \emph{Shared Folders} kann man einfach Daten zwischen dem
Wirt und dem Gast austauschen. Diese werden in den Einstellungen der VM entweder für die aktuelle Sitzung oder dauerhaft aktiviert. Außerdem muß mindestens ein Verzeichnis des Wirtssystems ausgewählt werden, auf welches die VM Zugriff erhält.

Der Zugriff innerhalb der VM erfolgt dann abhängig vom installierten Betriebssystem. Unter Linux werden die konfigurierten Shared Folders in ein Verzeichnis im Dateisystem eingebunden (\href{http://pubs.vmware.com/workstation-10/topic/com.vmware.ws.using.doc/GUID-AB5C80FE-9B8A-4899-8186-3DB8201B1758.html}{1}).

Die konfigurierten \emph{Shared Folders} finden Sie im Abschnitt Geräte des Datei-Browsers unter dem Eintrag \texttt{hgfs}.

\subsection*{Nacharbeiten bei Update des (Gast-)Systems}
Wenn man im Gast ein Online-Update ausgeführt hat und dabei der \emph{Kernel}\footnote{Die zentrale Komponente eines Betriebssystems} ausgetauscht wurde, so funktionieren zunächst die Erweiterungen des Gastsystems für VMware nicht mehr - dies betrifft z.B. die automatische Größenanpassung des Bildschirms oder der Übergang des Mauszeigers.

\subsubsection*{Abhilfe}
Um die Gast-Erweiterungen zu aktualisieren, führt man in der Konsole den Befehl \texttt{sudo vmware-config-tools.pl --default} aus. Die Option bewirkt, das nicht alle Fragen manuell beantwortet werden müssen, es muß lediglich das Benutzerpaßwort eingegeben werden.

\section*{Erste Schritte}
Schließen Sie die USB-Festplatte an einen Hochschul-PC an. Navigieren Sie im Wirtssystem in das VM-Verzeichnis auf der externen Platte und starten Sie die virtuelle Maschine durch Doppelklick. Es öffnet sich ein Fenster, welches den Bildschirminhalt der VM zeigt.


\begin{figure}[H]
	\includegraphics[width=\textwidth]{bsf-linuxmint.png}
	\caption{Nach dem Start sehen Sie die Oberfläche des installierten LinuxMint-Betriebssystems in der Version 17 (mate).}
\end{figure}

Um die VM wieder auszuschalten, verwenden Sie wie bei einem
 richtigen PC die entsprechende Funktion des Betriebssystems. 
 
 Das angemeldete Benutzerkonto heißt \texttt{student}, das Initialpaßwort
  lautet \texttt{123456}.

		 \subsection*{Verschlüsselung}
		 \emph{Wichtig:} Innerhalb der virtuellen
		 Maschine fallen mit der Zeit personenbezogene Daten an,
			auch Paßwörter werden auf der virtuellen Festplatte gespeichert.
			 Gehen Sie deshalb sorgsam mit der USB-Festplatte um.

		 Im Idealfall sollte die komplette virtuelle Festplatte aus 
		 Sicherheitsgründen verschlüsselt sein (Whole-Disk-Encryption).
		 
		 Damit das Image jedoch nicht zu groß wird, ist lediglich die Verschlüsselung
		 für das Benutzerverzeichnis aktiviert. Daher ist nach dem Systemstart
		 das Benutzerpaßwort einzugeben (voreingestellt ist \texttt{123456}). 
		 Es empfiehlt sich, dieses zu verändern - \emph{wichtig: Wenn Sie Ihr Passwort vergessen, sind die Daten unwiderbringlich verloren!}
		 
%TBD: Hinweis auf vmware-toolbox (BSF?)

\subsection*{Proxy} 
% s. http://www.ubuntugeek.com/how-to-configure-ubuntu-desktop-to-use-your-proxy-server.html
% s. http://wiki.ubuntuusers.de/Proxyserver
Konfigurieren Sie nun zunächst die Internetverbindung; an den PCs der Hochschule muß hier ein Proxy-Server eingestellt werden. Um ihn nutzen zu können, benötigen Sie ihren Benutzernamen sowie das zugehörige Paßwort. In der Benutzeroberfläche finden Sie die Proxyeinstellungen direkt auf dem Desktop (Netzwerkvermittlung). Gehen Sie wie folgt vor:
\begin{enumerate}
	\item Wählen Sie die Option ``Manuelle Proxy-Konfiguration''.
	\item Tragen Sie in das Feld ``HTTP-Proxy'' \texttt{10.100.16.65} ein.
	\item Im Feld ``Port'' tragen Sie \texttt{3128} ein.
	\item Drücken Sie neben diesem Feld auf ``Details''.
	\item Setzen Sie den Haken bei ``Legitimation erforderlich''.
	\item Tragen Sie in die beiden Felder Ihren DHBW-Benutzernamen sowie das zugehörige Paßwort ein.
	\item Schließen Sie den Authentifizierungsdialog.
	\item Tragen Sie in den Feldern hinter ``Sicherer HTTP-Proxy'' dasselbe ein wie eine Zeile darüber.
	\item Schließen Sie die Proxy-Einstellungen, indem Sie zweimal auf Schließen drücken. 
% Zur Not Zeile aus /etc/apt/apt.conf entfernen...
\end{enumerate}

Testen Sie die Einstellungen mit dem Browser, indem Sie eine beliebige Internetseite öffnen.

Sie können die VM natürlich auch an anderen PCs betreiben, die nicht über einen Proxy mit dem Internet verbunden sind (z.B. Ihrem Laptop). Schalten Sie dann den Proxy wieder aus, indem Sie in den Proxy-Einstellungen die Option ``Direkte Internet-Verbindung'' aktivieren. Generell bestimmt die Netzwerkkonfiguration des Wirtssystems, ob ein Proxyserver eingestellt werden muß oder nicht.

\subsection*{Kommandozeile}
Im Anwendungsmenü finden Sie unter Terminal die Kommandozeile. Sie dient unter Linux traditionell dazu, effektiv mit dem System zu interagieren. Funktionen, die sonst mühsam über mehrere Mausklicks gesucht werden müssen, lassen sich hier schnell und einfach ausführen.

\begin{minted}[frame=lines]{bash}
# Liste der Softwarepakete aus dem Internet aktualisieren
$ sudo apt-get update
# Systemupdate durchfuehren
$ sudo apt-get -y dist-upgrade
\end{minted}

Wenn Sie bereits mit Linux oder Unix gearbeitet haben, fallen Ihnen vielleicht einige Besonderheiten im Bezug auf das Verhalten der Kommandozeile auf. Die einzelnen Konfigurationsoptionen können Sie bei Bedarf in den Dateien \texttt{$\mathtt{\sim}$/.bashrc} sowie \texttt{$\mathtt{\sim}$/.bashrc\_custom} nachlesen. Sie betreffen unter anderem das Aussehen der Eingabeaufforderung sowie Abkürzungen für Befehle und das Verhalten beim Überschreiben von Dateien.

Eine ausführliche Einführung für Linux erhalten Sie im ersten Labortermin. Für den Einstieg bieten sich der \href{http://www.linuxmint.com/documentation/user-guide/MATE/english\_17.0.pdf}{Linux Mint User Guide} sowie die Seite \href{http://community.linuxmint.com/}{community.linuxmint.com} an.
\newpage
\section*{Häufige Fragen und Probleme}
\subsection*{Betrieb mit älterem VMware Player}
In den Laborräumen der DHBW sind teilweise veraltete Versionen des VMware Players installiert. Wenn Sie beim Starten der VM daher einen Fehler bekommen, gehen Sie wie folgt vor:
\begin{enumerate}
\item Kopieren Sie im Verzeichnis, indem sich die VM befindet, die Konfigurationsdatei der VM (Endung \texttt{.vmx}).
\item Öffnen Sie diese Kopie mit einem Texteditor\footnote{z.B. Wordpad, \emph{nicht} Notepad} (Rechtsklick->Öffnen mit...) und ändern folgende Einträge:
\begin{minted}[frame=lines]{bash}
config.version = "7"
virtualHW.version = "8"
\end{minted}
\item Speichern und schließen Sie die Datei.
\item Benutzen Sie ab sofort diese Kopie für den Betrieb mit älteren VMware Playern; sinnvollerweise benennen Sie die Datei auch dementsprechend\ldots
\end{enumerate}

\subsection*{Betrieb mit VirtualBox}
Um die VM mit VirtualBox (z.B. auf einem Mac) zu betreiben, erstellen Sie dort eine neue VM\footnote{Wenn Sie die USB-Festplatte nutzen, muß diese mit ExFAT formatiert sein.}:
\begin{enumerate}
\item Geben Sie der VM einen Namen (z.B. ET-VM), wählen als Typ Linux und als Version Ubuntu 64-bit.
\item Wählen Sie als Hauptspeichergröße mindestens 1GB (=1024MB).
\item Klicken Sie auf die Option ``Vorhandene Festplatte verwenden'' und wählen über den Knopf rechts daneben die virtuelle Festplatte der vorhandenen VM aus.

\emph{Wichtig:} Das Verzeichnis, indem sich die virtuelle Festplatte befindet, kann ab sofort nicht mehr ohne größeren Aufwand verschoben werden. Insofern entscheiden Sie bitte, ob die VM auf der USB-Platte verbleiben soll oder Sie sie auf Ihre interne Festplatte kopieren.
\item Abschließend klicken Sie auf ``Erzeugen'', danach ist Ihre VM einsatzbereit.

\end{enumerate}

\subsection*{Alles ganz klein? (HiDPI)}
Beim Betrieb an einem Host-Rechner mit hochauflösendem Display ``HiDPI'' kann die VM so eingestellt werden, daß die Darstellung angepaßt wird.

Starten Sie dazu die Einstellungen, dann Schreibtischeinstellungen->Fenster. Stellen Sie die Benutzerschnittstellenskalierung von Automatisch auf doppelt (HiDPI).

\subsection*{Code::Blocks}
Wenn beim Ausführen einer Konsolen-Anwendung ein Fehler auftritt, prüfen Sie die Einstellungen für den Terminal-Emulator (Settings->Environment...->Terminal to launch console programs:); diese sollte \texttt{mate-terminal --disable-factory -t \$TITLE -x} lauten 

%TODO:
%\item Nützliche Tips zum pfleglichen Umgang mit der USB-Platte
%\item SSH-Key erzeugen (braucht es das?)
%\item Mac-Adresse ändern
%\item \ldots
%\end{itemize}
%

\end{document}
