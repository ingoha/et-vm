#!/bin/bash

# Autor: Ingo Haschler <lehre@itih.de>

rm -rf ~/.cache/*

sudo apt update
sudo apt -y install wine winetricks libntlm0

wget http://friedrich-folkmann.de/papdesigner/v2.2.0.8.08/PapDesigner-Setup.exe -O /tmp/papdesigner-setup.exe
# Obacht: Dauert...
WINEARCH=win32 WINEPREFIX=~/.wine-papdesigner winetricks --force dotnet45 corefonts
# Silent install, cf. https://nsis.sourceforge.io/Docs/Chapter4.html#silent
WINEPREFIX=~/.wine-papdesigner wine /tmp/papdesigner-setup.exe /S

rm -rf ~/.cache/*
