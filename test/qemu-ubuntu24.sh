truncate -s 64m varstore.img
truncate -s 64m efi.img
dd if=/usr/share/qemu-efi-aarch64/QEMU_EFI.fd of=efi.img conv=notrunc
wget https://cloud-images.ubuntu.com/noble/current/noble-server-cloudimg-arm64.img
virt-customize -a noble-server-cloudimg-arm64.img --root-password password:coolpass
sudo qemu-system-aarch64 \
 -m 4096 \
 -smp 4 \
 -cpu cortex-a76 \
 -accel tcg,thread=multi \
 -M virt \
 -device virtio-gpu-pci \
 -object rng-random,filename=/dev/urandom,id=rng0 -device virtio-rng-pci,rng=rng0 \
 -drive if=pflash,format=raw,file=efi.img,readonly=on \
 -drive if=pflash,format=raw,file=varstore.img \
 -drive if=none,file=noble-server-cloudimg-arm64.img,id=hd0 \
 -device virtio-blk-device,drive=hd0 \
 -netdev type=user,id=net0,hostfwd=tcp::5555-:22 \
 -device virtio-net-device,netdev=net0 \
 -device qemu-xhci \
 -device usb-tablet \
 -device usb-kbd \
 -device virtio-serial-pci \
 -chardev socket,id=agent0,path=agent.sock,server=on,wait=off \
 -device virtserialport,chardev=agent0,name=org.qemu.guest_agent.0 \
 -chardev qemu-vdagent,id=ch1,name=vdagent,clipboard=on,mouse=off \
 -device virtio-serial-pci \
 -device virtserialport,chardev=ch1,id=ch1,name=com.redhat.spice.0 \
 -fsdev local,id=fsdev0,path=$HOME/shared,security_model=mapped-xattr \
 -device virtio-9p-pci,fsdev=fsdev0,mount_tag=Shared